import networkx as nx
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render

from molecules.api.int_act_rest_api import IntActConnect
from molecules.calculus import calculate_degree, clustering, degree_distribution_plot, create_pyvis_graph, \
    data_for_vis_graph, find_shortest_path, create_graph
from molecules.models import InteractionsModel
from molecules.models import NodesParametersModel


def find_interactions(request):
    """
    Widok służący do uzyskiwania danych o interakcjach dla wpisanej frazy. Wybrane dane z JSONA są zapisywane do bazy
    PostgresQL.
    :param request:
    :return:
    """

    if 'lvl' in request.GET:
        if request.GET.get('lvl') != '' and int(request.GET.get('lvl')) > 1:
            # IntActConnect().get_interaction_data(request)
            find_next_level_interaction(request)
        else:
            IntActConnect().get_interaction_data(request)
    all_interactions = IntActConnect().get_interaction_data(request)
    calculate_degree()
    split_interaction()
    return render(request, 'interactions/interactions.html', {"all_interactions": all_interactions})


def delete(request):
    """
    Widok usuwający dane z tabel.
    :param:
    :return:
    """
    InteractionsModel.objects.all().delete()
    NodesParametersModel.objects.all().delete()
    return render(request, 'interactions/interactions_table.html',
                  {"all_interactions": InteractionsModel.objects.all()})


def find_next_level_interaction(request):
    """
    Funkcja pobierająca kolejne dane dla sąsiednich węzłów
    :param request:
    :return:
    """
    # input_data = request.GET.get('protein_name')
    # if 'lvl' in request.GET:
    IntActConnect().get_more_int(request.GET.get('lvl'), request)



def split_interaction():
    """
    Funkcja rodzielająca molekuły w interakcji na osobne recordy w tabeli 'nodes_parameters'
    :return:
    """
    interactions = InteractionsModel.objects.all()
    for i in interactions:
        if NodesParametersModel.objects.filter(molecule=i.moleculeA).count() == 0:
            NodesParametersModel(
                molecule=i.moleculeA,
                degree=i.degree_A,
                clustering_coefficient=3.00,
                interaction_id=i.acA
            ).save()
        if NodesParametersModel.objects.filter(molecule=i.moleculeB).count() == 0:
            NodesParametersModel(
                molecule=i.moleculeB,
                degree=i.degree_B,
                clustering_coefficient=3.00,
                interaction_id=i.acB
            ).save()


def interactions_table(request):
    all_interactions = InteractionsModel.objects.all()
    return render(request, 'interactions/interactions_table.html', {"all_interactions": all_interactions})


def interaction_detail(request, id):
    """
    Widok wyświetlający szczegółowe dane na temat interakcji i molekuł wchodzących w ich skład.
    :param request:
    :param id:
    :return:
    """
    calculate_degree()
    interaction = InteractionsModel.objects.get(pk=id)
    return render(request, 'interactions/detail.html', {'interaction': interaction})


def measures(result):
    """
    Widok zawierający obliczone parametry.
    :param result:
    :return:
    """
    clustering()
    degree_distribution_plot()
    parameters = NodesParametersModel.objects.order_by('-degree')
    # create_pyvis_graph()
    return render(result, 'interactions/molecule_parameters.html', {'parameters': parameters})


def graph_view(result):
    """
    widok zawierający interaktywny graf, wyświetlający dane dla poszczególnych węzłów.
    :param result:
    :return:
    """
    json_data, assortativity, amount_of_nodes, amount_of_edges, av_clustering = data_for_vis_graph()
    return render(result, 'interactions/graph_view.html',
                  {'json_data': json_data,
                   'assortativity': assortativity,
                   'nodes_amount': amount_of_nodes,
                   'amount_of_edges': amount_of_edges,
                   'av_clustering': av_clustering})


def shortest_path(request):
    if request.is_ajax():
        source = request.GET.get('source')
        target = request.GET.get('target')
        path = find_shortest_path(source, target)
        # length = request.POST(len(path))
        return JsonResponse({'result': 'OK', 'data': {'path': path, 'long': len(path)}})
    return JsonResponse({'result': 'Not OK', 'data': {'path': ''}, 'long': '-'})


def select_node_interaction(result, id_ac):
    try:
        interactions = InteractionsModel.objects.filter(Q(moleculeA=id_ac) | Q(moleculeB=id_ac))
        nodes_names = []
        for i in interactions:
            if id_ac == i.moleculeB:
                nodes_names.append(i.moleculeA)
            if id_ac == i.moleculeA:
                nodes_names.append(i.moleculeB)
        parameters = NodesParametersModel.objects.filter(molecule__in=nodes_names).order_by('-degree')
        return render(result, 'interactions/molecule_parameters.html', {'parameters': parameters})
    except ObjectDoesNotExist:
        parameters = None
        return render(result, 'interactions/molecule_parameters.html', {'parameters': parameters})


def histogram(result):
    json_data = degree_distribution_plot()
    return render(result, 'interactions/degree_histogram_view.html', {'json_data': json_data})
