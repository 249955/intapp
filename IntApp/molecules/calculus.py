"""
Tutaj znajdują się narzędzia matematyczne do obliczania parametrów dla sieci
"""

import networkx as nx

from molecules.models import InteractionsModel
from molecules.models import NodesParametersModel
from networkx.readwrite import json_graph
import matplotlib.pylab as plt
from pyvis.network import Network  # biblioteka do rysowania ładnych interaktywnych sieci


# from flask import Flask, jsonify, render_template, request


def create_graph(graph_type, **kwargs):
    """
    Funkcja generująca nieukierunkowany graf dla węzłów z bazy.
    :return: graf dla sieci z bazy
    """
    color = kwargs.get('color', None)
    color_map = {}
    if graph_type == 'DiGraph':
        G = nx.DiGraph()
    else:
        G = nx.Graph()

    for i in InteractionsModel.objects.all():
        if color == 'return':
            if i.typeA == 'protein':
                color_map.update({i.moleculeA: 'Aqua'})
            elif i.typeA == 'small molecule':
                color_map.update({i.moleculeA: 'pink'})
            elif i.typeA == 'rna':
                color_map.update({i.moleculeA: 'purple'})
            elif i.typeA == 'ds dna':
                color_map.update({i.moleculeA: 'green'})
            elif i.typeA == 'ss dna':
                color_map.update({i.moleculeA: 'green'})
            if i.typeB == 'protein':
                color_map.update({i.moleculeB: 'Aqua'})
            elif i.typeB == 'small molecule':
                color_map.update({i.moleculeB: 'pink'})
            elif i.typeB == 'rna':
                color_map.update({i.moleculeB: 'purple'})
            elif i.typeB == 'ds dna':
                color_map.update({i.moleculeB: 'green'})
            elif i.typeB == 'ss dna':
                color_map.update({i.moleculeB: 'green'})
            else:
                color_map.update({i.moleculeA: 'orange'})
                color_map.update({i.moleculeB: 'orange'})
            if i.moleculeA == 'MUSK':
                color_map.update({i.moleculeA: 'red'})
            elif i.moleculeB == 'MUSK':
                color_map.update({i.moleculeB: 'red'})
            elif i.moleculeA == 'AGRN':
                color_map.update({i.moleculeA: 'red'})
            elif i.moleculeB == 'AGRN':
                color_map.update({i.moleculeB: 'red'})
            elif i.moleculeB == 'LRP4':
                color_map.update({i.moleculeB: 'red'})
            elif i.moleculeA == 'LRP4':
                color_map.update({i.moleculeA: 'red'})
            elif i.moleculeA == 'DOK7':
                color_map.update({i.moleculeA: 'red'})
            elif i.moleculeB == 'DOK7':
                color_map.update({i.moleculeB: 'red'})
            elif i.moleculeA == 'COL13A1':
                color_map.update({i.moleculeA: 'red'})
            elif i.moleculeB == 'COL13A1':
                color_map.update({i.moleculeB: 'red'})
        G.add_edge(i.moleculeA, i.moleculeB, weight=1)
    if color == 'return':
        return G, color_map
    # nx.draw_networkx_nodes(G, nx.spring_layout(G), nodelist=G.nodes, node_color=color_map)

    else:
        return G


def calculate_degree():
    """
    Funkcja licząca stopień dla węzła w sieci.
    :param
    :return:
    """
    G = create_graph('DiGraph')
    interactions = InteractionsModel.objects.all()
    for interaction in interactions:
        interaction.degree_A = G.degree[interaction.moleculeA]
        interaction.save()
        interaction.degree_B = G.degree[interaction.moleculeB]
        interaction.save()


def clustering():
    """
    Funkcja zwracająca współczynniki zgrupowania (cluster coefficient).
    :param:
    :return: tablice zgrupowania
    """
    G = create_graph('DiGraph')
    clusters = nx.clustering(G)
    for key, value in clusters.items():
        molecule = NodesParametersModel.objects.get(molecule=key)
        molecule.clustering_coefficient = value
        molecule.save()


def degree_distribution_plot():
    """
    Funkcja obliczająca dystrybucję stopnia sieci.
    :param result:
    :return:
    """
    G = create_graph('Graph')
    degree_sequence = sorted([d for n, d in G.degree()], reverse=True)
    histogram_data = [degree_sequence.count(x) for x in degree_sequence]
    plt.figure()
    plt.grid(True)
    plt.plot(degree_sequence, histogram_data, 'ro-')  # lub loglog
    plt.xlabel('Degree')
    plt.ylabel('Number of nodes')
    plt.savefig('degree_dist.jpg')
    plt.close()

    return {'degree_sequence': degree_sequence, 'histogram_data': histogram_data}


def find_shortest_path(source, target):
    """
    Funkcja zwracająca najkrótszą ścieżkę dla wybranych węzłów
    :param clicked_nodes: słownik wybranych węzłów
    :param source: węzeł startowy
    :param target: węzeł docelowy
    :return:
    """
    G = create_graph('Graph')
    try:
        path = nx.shortest_path(G, source, target)
        print(path)
        return path
    except nx.NodeNotFound:
        return 'Nie znaleziono węzła'


def data_for_vis_graph():
    G, color_map = create_graph('DiGraph', color='return')
    json_data = json_graph.node_link_data(G, attrs=dict(source='from', target='to', link='links', name='id', key='key'))
    assortativity = round(nx.degree_assortativity_coefficient(G), 2)
    amount_of_nodes = NodesParametersModel.objects.all().count()
    amount_of_edges = len(nx.edges(G))
    # print(amout_of_eges)
    av_clustering = round(nx.average_clustering(G), 4)
    for node in json_data['nodes']:
        node['label'] = node['id']
        node['color'] = color_map.get(node['id'])
    return json_data, assortativity, amount_of_nodes, amount_of_edges, av_clustering


def create_pyvis_graph():
    G = create_graph('DiGraph')
    nt = Network('1000px', '1000px')
    nt.from_nx(G)
    if len(G.nodes) != 0:
        nt.save_graph('IntApp/molecules/templates/charts/network_graph.html')
    else:
        nt.add_node(0, label='brak danych', color='red')
        nt.save_graph('IntApp/molecules/templates/charts/network_graph.html')
