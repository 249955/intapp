"""Klasa odpowiadająca za połączenie z bazą intAct, zawiera wszystkie zapytania API dla serwisu IntAct"""
import http

import requests
from django.db.models import Q
from django.http import HttpResponse

from molecules.models import InteractionsModel


class IntActConnect(HttpResponse):
    url = 'https://www.ebi.ac.uk/intact/'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_find_interaction(self, query):
        try:
            response = requests.get(self.url + 'ws/interaction/findInteractions/' + str(query), timeout=2)
        except (ConnectionError) as e:  # This is the correct syntax
            print(e)
            r = "No response"
        if (
                response.status_code != 204 and
                response.headers["content-type"].strip().startswith("application/json")
        ):

            try:
                return response.json()
            except (ConnectionError, ValueError) as e:  # This is the correct syntax
                print(e)
                r = "No response"
        return response.json()

    def get_interaction_data(self, request):
        all_interactions = {}
        if 'protein_name' in request.GET:
            protein_name = request.GET.get('protein_name')
            all_interactions = self.data_parser(protein_name)
        return all_interactions

    def get_next_interaction(self, protein_name):
        all_interactions = {}
        if protein_name is not None:
            all_interactions = self.data_parser(protein_name)
        return all_interactions

    def data_parser(self, protein_name):
        all_interactions = {}
        data = self.get_find_interaction(protein_name)
        interactions = data['content']
        for i in interactions:
            if i["moleculeB"] is not None:
                if str(i["speciesA"]) == 'Homo sapiens' or str(i["speciesB"]) == 'Homo sapiens':
                    interactions_data = InteractionsModel(
                        ac=i["ac"],
                        binary_id=i["binaryInteractionId"],
                        idA=i["idA"],
                        idB=i["idB"],
                        acA=i["acA"],
                        acB=i["acB"],
                        moleculeA=i["moleculeA"],
                        moleculeB=i["moleculeB"],
                        intact_name_A=i["intactNameA"],
                        intact_name_B=i["intactNameB"],
                        mutationA=i["mutationA"],
                        mutationB=i["mutationB"],
                        aliasesA=i["aliasesA"],
                        aliasesB=i["aliasesB"],
                        descriptionA=i["descriptionA"],
                        descriptionB=i["descriptionB"],
                        typeA=i["typeA"],
                        typeB=i["typeB"],
                        speciesA=i["speciesA"],
                        speciesB=i["speciesB"]
                    )
                    interactions_data.save()
                    all_interactions = InteractionsModel.objects.all().order_by('-id')
        return all_interactions

    def get_more_int(self, how_much, request):
        used_interaction = []
        if len(InteractionsModel.objects.all()) == 0:
            self.get_interaction_data(request)
            self.loop_parser(how_much, used_interaction)

        else:
            # used_interaction = [InteractionsModel.objects.values('ac')]
            used_interactions = InteractionsModel.objects.all()
            for interaction in used_interactions:
                used_interaction.append(interaction.ac)
            self.get_interaction_data(request)
            self.loop_parser(how_much, used_interaction)

    def loop_parser(self, how_much, used_interaction):
        for counter in range(int(how_much) - 1):
            for interaction in InteractionsModel.objects.all():
                if interaction.ac not in used_interaction:
                    self.data_parser(interaction.acA)
                    self.data_parser(interaction.acB)
                    used_interaction.append(interaction.ac)
            counter += 1

    def get_ac(self, interactions):
        """
         Funkcja zwracająca numer dostępu UniProt z bazy (Ac) dla molekuły A i B.
        :param interactions: lista interakcji
        :return: kod dostępu UniProt - typ [String]
        """
        ac_a = []
        ac_b = []
        for interaction in interactions:
            ac_a.append(interaction.acA)
            ac_b.append(interaction.acB)
        return [ac_a, ac_b]
