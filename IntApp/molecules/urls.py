from django.urls import path

from molecules import views

urlpatterns = [
    path('', views.interactions_table, name='get interactions'),
    path('find_interactions', views.find_interactions, name='get interactions'),
    path('details/<int:id>', views.interaction_detail, name='detail'),
    path('plot', views.degree_distribution_plot, name='plot'),
    path('measures/', views.measures, name='molecules measures'),
    path('delete/', views.delete, name='clear data'),
    path('node_interactions/<str:id_ac>', views.select_node_interaction, name='node interactions'),
    path('graph_view/', views.graph_view, name='graph view'),
    path('shortest_path/', views.shortest_path),
    path('histogram', views.histogram, name='histogram view'),
]

