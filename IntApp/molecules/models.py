from django.db import models


class InteractionsModel(models.Model):
    """Model przechowujący  dane dotyczące poszczególnych molekuł w bazie"""
    # django generuje id automatycznie
    ac = models.CharField(name='ac', null=True, max_length=200)
    binary_id = models.IntegerField(name='binary_id', null=True)
    idA = models.CharField(name='idA', null=True, max_length=200)
    idB = models.CharField(name='idB', null=True, max_length=200)
    acA = models.CharField(name='acA', null=True, max_length=200)
    acB = models.CharField(name='acB', null=True, max_length=200)
    moleculeA = models.CharField(name='moleculeA', null=True, max_length=200)
    moleculeB = models.CharField(name='moleculeB', null=True, max_length=200)
    intact_name_A = models.CharField(name='intact_name_A', null=True, max_length=200)
    intact_name_B = models.CharField(name='intact_name_B', null=True, max_length=200)
    mutationA = models.CharField(name='mutationA', null=True, max_length=300)
    mutationB = models.CharField(name='mutationB', null=True, max_length=300)
    aliasesA = models.TextField(name='aliasesA', null=True, max_length=1000)
    aliasesB = models.TextField(name='aliasesB', null=True, max_length=1000)
    descriptionA = models.CharField(name='descriptionA', null=True, max_length=400)
    descriptionB = models.CharField(name='descriptionB', null=True, max_length=400)
    typeA = models.CharField(name='typeA', null=True, max_length=100)
    typeB = models.CharField(name='typeB', null=True, max_length=100)
    speciesA = models.CharField(name='speciesA', null=True, max_length=200)
    speciesB = models.CharField(name='speciesB', null=True, max_length=200)
    degree_A = models.IntegerField(verbose_name="stopień molekuły A", name='degree_A', null=True)
    degree_B = models.IntegerField(verbose_name="stopień molekuły B", name='degree_B', null=True)


class NodesParametersModel(models.Model):
    """Model przechowujący pomiary dla molekuł."""
    molecule = models.CharField(name='molecule', verbose_name='nazwa molekuły', null=False, default='-', max_length=200)
    degree = models.IntegerField(name='degree', verbose_name='stopień węzła', null=True, default=0)
    clustering_coefficient = models.DecimalField(name='clustering_coefficient', verbose_name='współczynnik zgrupowania',
                                                 null=True, max_digits=5, decimal_places=2)
    interaction_id = models.CharField(name='interaction_id',
                                      verbose_name='id interakcji z tabeli interakcji (acA lub acB)',
                                      null=False, default='-', max_length=50)
