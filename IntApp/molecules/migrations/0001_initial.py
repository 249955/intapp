# Generated by Django 3.2.8 on 2021-10-24 21:53

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='InteractionsModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ac', models.CharField(max_length=200, null=True)),
                ('binary_id', models.IntegerField(null=True)),
                ('idA', models.CharField(max_length=200, null=True)),
                ('idB', models.CharField(max_length=200, null=True)),
                ('acA', models.CharField(max_length=200, null=True)),
                ('acB', models.CharField(max_length=200, null=True)),
                ('moleculeA', models.CharField(max_length=200, null=True)),
                ('moleculeB', models.CharField(max_length=200, null=True)),
                ('intact_name_A', models.CharField(max_length=200, null=True)),
                ('intact_name_B', models.CharField(max_length=200, null=True)),
                ('mutationA', models.CharField(max_length=200, null=True)),
                ('mutationB', models.CharField(max_length=200, null=True)),
                ('aliasesA', models.CharField(max_length=200, null=True)),
                ('aliasesB', models.CharField(max_length=200, null=True)),
                ('descriptionA', models.CharField(max_length=200, null=True)),
                ('descriptionB', models.CharField(max_length=200, null=True)),
                ('typeA', models.CharField(max_length=100, null=True)),
                ('typeB', models.CharField(max_length=100, null=True)),
                ('speciesA', models.CharField(max_length=200, null=True)),
                ('speciesB', models.CharField(max_length=200, null=True)),
            ],
        ),
    ]
