# Generated by Django 3.2.7 on 2021-11-14 19:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('molecules', '0004_alter_nodesparametersmodel_clustering_coefficient'),
    ]

    operations = [
        migrations.AddField(
            model_name='nodesparametersmodel',
            name='interaction_id',
            field=models.CharField(default='-', max_length=50, verbose_name='id iterakcji z tabeli interakcji'),
        ),
    ]
