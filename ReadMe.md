https://gitlab.com/249955/intapp/-/wikis/Konfiguracja-%C5%9Brodowiska
1. Pobieramy projekt
2. Należy pobrać biblioteki z pliku calculus(NetworkX, pyvis, matplotlib)
3. Przez menadżera pakietów instalujemy: pip install psycopg2
4. W file -> settings -> Languages & Frameworks -> Django ustawiamy:
![image](uploads/c681f5992f3a2573c429e160b588108e/image.png)
5. Konfiguracja:
![image](uploads/82b7c44d13b856115231e41e058ebd87/image.png)
![image](uploads/e3970f6dade59db03d146cb406ef159b/image.png)
6. W aplikacji pgAdmin tworzymy baze danych dla PostgreSQL 13 o nazwie IntAppDB, ustawione hasło zapisujemy w 
 IntApp/settings.py 
7. `<ctrl + alt + r>` uruchamiamy terminal komend Django i wpisujemy: migrate

8. Uruchamiamy, aplikacja powinna w terminalu wyświetlić hiper łącze do  strony dla ustawionego localhosta.
